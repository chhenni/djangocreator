#!/usr/bin/env bash

function act() {
	echo "Activating virtualenv"
	source ./scripts/activate.sh || true
}

function dact() {
	echo "Deactivating virtualenv"
	deactivate
}

function setup_git(){
	echo "Initiating repository"
	git init
	git add .gitignore
	git add -A .	
}

function copy_files(){
	echo "Copying in files"
	cp -r deploy.sh scripts $1
	cp gitignore $1/.gitignore
	cp -r templates static $1
}

function create_missing(){
	touch requirements.txt
}

function configure_project(){
	echo "Configuring project"
	cd -
	cat settings_extras.py >> $1/$2/settings.py
	cp wsgi.py $1/$2/wsgi.py
	sed -i -e 's/PROJECTNAME/'$2'/g' $1/$2/wsgi.py
	cd -
}

scriptdir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

if [ -z "$1" ] || [ -z "$2" ] ; then
	echo "Run setup.sh path projectname"
else

	mkdir -p $1
	cd $1
	target="$(pwd)"
	cd -
	echo "Creating environment for project: $2 in $target"

	cd $scriptdir
	if [ "$(ls -A $target)" ]; then
		echo "Directory is not empty"
	else
		echo "Setting up directory $target"
		copy_files $target
		cd $target
		echo "$(pwd)"
		setup_git
		create_missing
		echo "Setting up virtualenv"
		bash ./scripts/setup_virtualenv.sh
		pwd
		act
		pwd
		echo "Installing django"
		pip install django
		echo "Creating project"
		django-admin startproject $2 .
		configure_project $target $2
		dact
		echo "Freezing packages"
		bash ./scripts/freeze_packages.sh
	fi
fi