virtualenv env

if [ -f env/Scripts/activate ];
then
	source env/Scripts/activate
fi

if [ -f env/bin/activate ];
then
	source env/bin/activate
fi

pip install -r requirements.txt
deactivate