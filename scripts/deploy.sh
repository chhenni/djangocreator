#!/usr/bin/env bash
echo "Deploying to $1"

read -p "Are you sure? (y/n)" -N 1
echo 
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi

if [ "$#" -gt 0 ] ; then
	echo "Initiating deployment"
	sudo mkdir $1
	exclude="" 
	sudo rsync -av --exclude='.git' --exclude='PosTracker/settings.py' . $1
	sudo chown -R www-data $1
	cd $1
	sudo -u www-data python manage.py collectstatic

	read -p "Do you want to run syncdb? (y/N)" -N 1
	echo
	if [[ $REPLY =~ ^[Yy]$ ]]
	then
		sudo -u www-data python manage.py syncdb
	fi
	
	read -p "Do you want to apply migrations? (Y/n)" -N 1
	echo
	if [[ ! $REPLY =~ ^[Nn]$ ]]
	then
		sudo -u www-data python manage.py migrate
	fi
	
	read -p "Do you want to restart apache? (y/N)" -N 1
	echo
	if [[ $REPLY =~ ^[Yy]$ ]]
	then
		sudo service apache2 restart
	fi

fi






